Virtual PASTTREC
================

.. MDCTDC_PT
.. =========

.. py:class:: MDCTDC_PT

    A class for handling the individual configurations for each PASTTREC.

    **Attributes**

    .. py:attribute:: gain 
        :type: float

        Value of gain for this PASTTREC. Acceptable values is only: 0.67, 1, 2, 4. 
    .. py:attribute:: pt
        :type: float

        Value of peaking time for this PASTTREC. Acceptable values is only: 10, 15, 20, 35.
    .. py:attribute:: thr
        :type: float

        Value of threshold for this PASTTREC. Acceptable values are from range from 0 to 2^7.
    .. py:attribute:: baselines
        :type: list(float)

        Values of baseline for each channel of this PASTTREC. Acceptable values are from range from 0 to 31.
    .. py:attribute:: gain_map
        :type: dict(float, int)
        :value: {0.67: 0b11, 1: 0b10, 2: 0b01, 4: 0b00}

        Bit mask for each value of gain. 

        .. caution:: Not for end user.

    .. py:attribute:: pt_map
        :type: dict(float, int)
        :value: {10: 0b00, 15: 0b01, 20: 0b10, 35: 0b11}

        Bit mask for each value of peaking time. 

        .. caution:: Not for end user.

    **Methods**
    
    .. py:method:: __init__(gain=4,peaktime=10,thr=0x08,baselines=[0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f])
        
        Constructor

        :param float gain: A value of gain.
        :param float peaktime: A value of peaking time.
        :param float thr: A value of threshold.
        :param list(float) baselines: Values of baseline levels.
        :returns: The instance of :py:class:`MDCTDC_PT` object.

    .. py:method:: setBaselines(new)
        
        Set new values for baselines. Automatically checks validity.

        :param list(float) new: New baselines.
        :returns: None

    .. py:method:: setBaseline(ch, value)
        
        Set a new value for one specific baseline. Automatically checks validity.

        :param float ch: Channel number.
        :param float value: New value of baseline for channel ch.
        :returns: None

    .. py:method:: setGain(gain)
        
        Set a new value for gain. Automatically checks validity.

        :param float gain: New value of gain.
        :returns: None

    .. py:method:: setPt(peaktime)
        
        Set a new value for peaking time. Automatically checks validity.

        :param float peaktime: New value of peaking time.
        :returns: None
    
    .. py:method:: setThr(thr)
        
        Set a new value for threshold. Automatically checks validity.

        :param float thr: New value of threshold.
        :returns: None

    .. py:method:: resetConfig()
        
        Set all PASTTREC settings inside this MDCTDC_PT object to the default value.

        :returns: None

    .. py:method:: getImage()
        
        Create an image of current PASTTREC settings.

        :returns: A dictionary, that represents the actual PASTTREC registers content corresponding to its settings. Keys are register addresses, values are actual content.
        :rtype: dict(int, int)

        .. caution:: Not for end user. Use :py:meth:`Configuration.createImage` instead.
        
    .. py:method:: applySCCmd(cmd)
        
        Emulates the execution of slow control command on this virtual PASTTREC. Changes the settings according to the specified command.
        Only in write mode.

        :param int cmd: A slow control command, that should be applied. For details, see the PASTTREC documentation.
        :returns: None

        .. admonition:: Example
            
            A possible value of an emulated command:

            .. code-block:: python
                
                cmd = 0x5060F
        
    .. py:method:: applyDefaultConfSet(conf_set)
        
        The same as :py:meth:`applySCCmd`, but allows performing a list of commands executions, one by one.

        :param list(int) cmd: A list of slow control commands, that should be applied. For details, see the PASTTREC documentation.
        :returns: None

    .. py:classmethod:: read_configuration(TDC_str, pasttrec)

        Create an :py:class:`MDCTDC_PT` object and set its settings to the same as in real PASTTREC with a specified TDC address.
        Useful for reading data back from PASTTREC.

        :param str TDC_str: An address of assosiated TDC.
        :param int pasttrec: A PASTTREC number [0-3].
        :returns: An :py:class:`MDCTDC_PT` object with the same settings as in one of real ASICs.
        :rtype: :py:class:`MDCTDC_PT`

        .. include:: reset_warning_adm.rst

        .. hint::
            If data reading from all PASTTRECs is needed, take a look at :py:meth:`MDCTDC_FPGA.read_configuration` method.








