.. Python front-end for MDC PASTTRECs documentation master file, created by
   sphinx-quickstart on Mon Jan  3 15:10:45 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python front-end for MDC PASTTRECs documentation!
==============================================================

The source code can be found on my `GitLab <https://git.gsi.de/v.svintozelskyi/mdcupgrade_passttrec_python_frontend>`_.

The basic idea is to let the user manipulate both virtual configuration objects and real hardware via Jupyter Notebook.
The system uses the pasttrec_ctrl module as a port for physical interaction with real hardware using trbnet library.
The passtrec_conf module was created for simpler handling of different configurations and quick testing them. 

In addition to this documentation, examples of using different modules may be found in two Jupyter Notebooks:

* `Examples of usage <https://git.gsi.de/v.svintozelskyi/mdcupgrade_passttrec_python_frontend/-/blob/master/Notebooks/Examples%20of%20usage.ipynb>`_ 
* `How to configure your PASTTREC <https://git.gsi.de/v.svintozelskyi/mdcupgrade_passttrec_python_frontend/-/blob/master/Notebooks/How%20to%20configure%20your%20PASTTREC.ipynb>`_

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   pasttrec_ctrl
   pasttrec_conf


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
