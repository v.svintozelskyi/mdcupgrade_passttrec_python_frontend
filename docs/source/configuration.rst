Configuration object
====================

.. py:class:: Configuration

    The main class for handling all the PASTTREC configurations.

    **Attributes**

    .. py:attribute:: OEPs
        :type: list(MDCTDC_OEP)
        
        An array of all associated with this configuration OEP objects.

    **Methods**

    .. py:method:: __init__(mdc_tree=None, file=None)

        Constructor. Either mdc_tree or file must be specified. If both, the configuration will be loaded from a file.

        :param dict mdc_tree: A tree that contains addresses of all boards.
        :param str file: Path to the configuration file. 
        :returns: The instance of :py:class:`Configuration` class.
        :raises SyntaxError: If neither mdc_tree nor file specified.

        .. admonition:: Example
            
            The possible content of mdc_tree dictionary (0x8012 - OEP, 0x1820 and 0x1821 - FPGAs)

            .. code-block:: python
                
                mdc_tree = {'0x8012': ['0x1820', '0x1821']}

        .. hint::

            It's possible to use predefined functions to generate mdc_tree:

            .. code-block:: python

                mdc_tree = Helper.getConnectedMDCtree()
                # or
                mdc_active_tree = Helper.getActiveMDCsubtree()

    .. py:method:: generateDefault(mdctree)

        Default initialization of configuration object.

        :param dict mdc_tree: A tree that contains addresses of all boards.
        :returns: this object.
        :rtype: :py:class:`Configuration`

        .. caution:: Not for end user. Use :py:meth:`__init__` instead.
        
    .. py:method:: found_all_baselines(scanning_time = 0.2, plot=False, apply_res=False)

        Found the baselines for all FPGAs, present in the current configuration, using the noise method. 

        :param float scanning_time: Duration of each step during data acquisition.
        :param boolean plot: if true, plots the noise level versus baseline for each channel.
        :param boolean apply_res: if true, after the baselines will be found, they will be applied for each channel.
        :returns: this object.
        :rtype: :py:class:`Configuration`

        .. include:: reset_warning_adm.rst

    .. py:method:: writeConfigurationToFile(file)
    
        Save this configuration object to file.

        :param str file: path to a file
        :returns: this object.
        :rtype: :py:class:`Configuration`
            
    .. py:method:: readConfigurationFromFile(file)
    
        Initialization of configuration object from file. 

        :param str file: path to a file
        :returns: this object.
        :rtype: :py:class:`Configuration`

        .. caution:: Not for end user. Use :py:meth:`__init__` instead.

    .. py:method:: createImage(path)

        Create a flashable image from this configuration object and save it on the disk.

        :param str path: path to a folder, where the image should be placed.
        :returns: this object.
        :rtype: :py:class:`Configuration`

    .. py:method:: applyChanges(allowed_tdc=[])

        Temporary apply this configuration to selected TDCs. Kept until a power cycle or soft reset.

        :param list(str) allowed_tdc: list of TDCs, for which changes should be applied. If empty - apply for all boards in this configuration.
        :returns: this object.
        :rtype: :py:class:`Configuration`            

    .. py:staticmethod:: flashImage(path, flash_settings, TDCs=[])

        Flash image to selected TDCs.

        :param str path: Path to a saved image on disk, that should be flashed.
        :param str flash_settings: Path to flash_settings script with all required flags.
        :param list(str) TDCs: list of TDCs, that should be flashed. If empty - apply for all boards in this configuration.
        :returns: None.

        .. admonition:: Example
            
            The value of flash_settings, if flash_settings.pl in the same directory:

            .. code-block:: python
                
                flash_settings = "./flash_settings.pl -y"

            Option -y needed to avoid the termination of the process as user confirmation is required
            

