.. _pasttrec_conf:

Configuration module (pasttrec_conf.py)
=======================================

Contain the mechanisms for handling virtual PASTTREC configurations and effective testing them.

.. toctree::
    :maxdepth: 1
    
    Virtual PASTTREC <mdctdc_pt>
    Virtual FPGA <mdctdc_fpga>
    Virtual OEP <mdctdc_oep>
    Configuration object <configuration>
    Additional classes <additional>
