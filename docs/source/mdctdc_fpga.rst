Virtual FPGA (TDC)
==================

.. py:class:: MDCTDC_FPGA

    A class for handling all 4 PASTTREC configurations for every specific TDC.

    **Attributes**

    .. py:attribute:: addr 
        :type: str

        Address of this TDC
    .. py:attribute:: pasttrec 
        :type: list(MDCTDC_PT)

        An array of MDCTDC_PT objects.

    **Methods**
    
    .. py:method:: __init__(addr, pasttrec)

        Constructor

        :param str addr: TDC address.
        :param pasttrec: Array of MDCTDC_PT objects.
        :type pasttrec: list(:py:class:`MDCTDC_PT`)
        :returns: The instance of :py:class:`MDCTDC_FPGA` object.

    .. py:method:: found_baseline(scanning_time = 0.2)

        Found the baselines for all PASTTRECs of this TDC.
        The founded values are assigned only to the virtual PASTTRECs of this FPGA after scanning, but not to the real PASTTRECs. 

        :param float scanning_time: Duration of each step during data acquisition.
        :returns: None

        .. include:: reset_warning_adm.rst

    .. py:method:: getImage()

        Generate a flashable image of this TDC.

        :returns: The memory content of TDC for automatic applying of current virtual FPGA settings for real PASTTRECs during the power cycle. Keys are the addresses, values - corresponding content of each memory cell.
        :rtype: dict(int, int)

        .. caution:: Not for end user. Use :py:meth:`Configuration.createImage` instead.

    .. py:method:: resetConfig()

        Set all settings of all associated virtual PASTTRECs to the default values.

        :returns: None

    .. py:classmethod:: read_configuration(TDC_str)

        Create an MDCTDC_FPGA object and set its settings to the same as in real PASTTRECs of specified TDC.
        Useful for reading data back from PASTTREC.

        :param str TDC_str: Address of real TDC, from which the settings should read back.
        :returns: The instance of :py:class:`MDCTDC_FPGA`, with the same settings as in real TDC.
        :rtype: :py:class:`MDCTDC_FPGA`

        .. include:: reset_warning_adm.rst


