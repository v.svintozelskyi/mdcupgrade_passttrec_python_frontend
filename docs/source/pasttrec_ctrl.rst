Control module (pasttrec_ctrl.py)
==========================================

Contain all methods for physical interaction with real boards.

.. py:function:: spi(TDC_str,CONN,CHIP, data_list, **kwargs)

  Transmit data to a specific PASTTREC using old-style notation.  

  :param str TDC_str: Address of associated TDC.
  :param int CONN: Number of PASTTREC connection.
  :param int CHIP: Number of PASTTREC inside a connection.
  :param list(int) data_list: Data to be transmitted. Each entry should contain the address of the target register (bits from 11 to 8) and actual data (bits from 7 to 0)
    
  .. admonition:: Example 

    Content of data_list for changing registers 0x0, 0x1 and 0x2. Here the only actual data to be written is the word 0x12

    .. code-block:: python

      [
        0x012,
        0x112,
        0x212
      ]

  .. warning:: 
    Deprecated. Use :py:func:`spi_mdc_mbo` or :py:func:`spi_set_pasttrec_register` instead.

.. py:function:: spi_mdc_mbo(TDC_str,ADDR, data)

  Write data to a specific TDC register. Supports querying.

  :param str TDC_str: Address of associated TDC.
  :param int ADDR: Address of target register.
  :param int data: Data to be written.
  
  .. admonition:: Example

    Sending a soft reset command to a TDC 0x1800

    .. code-block:: python

      spi_mdc_mbo("0x1800", 0xAA00, 0x00000000)
    
    What is equivalent to command-line command:

    .. code-block:: bash

      trbcmd w 0x1800 0xaa00 0x00000000

  .. note::
    The full list of commands can be found on the `GitLab page <https://git.gsi.de/v.svintozelskyi/mdcupgrade_spi_pasttrec>`__ of a corresponding FPGA module.

.. py:function:: spi_set_pasttrec_register(TDC_str,pasttrec,reg, data)

  Set a value to a specific PASTTREC register.

  :param str TDC_str: Address of associated TDC.
  :param int pasttrec: Number of PASTTREC ASIC [0-3].
  :param int reg: Address of PASTTREC register [0x0 - 0xD].
  :param int data: Data to be written [0x00 - 0xFF].

  .. note::
    The full list of commands can be found on the `GitLab page <https://git.gsi.de/v.svintozelskyi/mdcupgrade_spi_pasttrec>`__ of a corresponding FPGA module.

  .. include:: reset_warning_adm.rst

.. py:function:: spi_set_pasttrecs_register(TDC_str,reg, data)

  The same as :py:func:`spi_set_pasttrec_register`, but for all associated with specific TDC PASTTRECs at once. 

  :param str TDC_str: Address of associated TDC.
  :param int reg: Address of PASTTREC register [0x0 - 0xD].
  :param int data: Data to be written [0x00 - 0xFF].

.. py:function:: load_set_to_pasttrec(TDC_str,pasttrec,addr,len)

  Load set of commands from module memory to one PASTTREC.

  :param str TDC_str: TDC address.
  :param int pasttrec: PASTTREC number
  :param int addr: Address of the first command in module memory.
  :param int len: Number of commands in a set.

  .. include:: reset_warning_adm.rst

.. py:function:: load_set_to_pasttrecs(TDC_str,addr,len)

  The same as :py:func:`load_set_to_pasttrec`, but for all PASTTRECs associated the given TDC at once.  

  :param TDC_str: TDC address.
  :type TDC_str: str
  :param addr: Address of the first command in module memory.
  :type addr: int
  :param len: Number of commands in a set.
  :type len: int  

.. py:function:: reset_board(TDC_str,CONN=0)

  Performs a soft reset of the PASTTREC FPGA module (without interruption of all other systems)

  :param str TDC_str: Address of associated TDC.
  :param int CONN: Not used. For back-compatibility only.

.. py:function:: reset_boards(TDC_strs,CONN=0)

  The same as :py:func:`reset_board`, but for a list of TDCs. 

  :param list(str) TDC_strs: An array of associated TDC's addresses.
  :param int CONN: Not used. For back-compatibility only.

.. py:function:: reset_board_by_name(board_name)

  The same as :py:func:`reset_board`.

  :param str board_name: Board name.

.. py:function:: set_threshold(TDC,CONN,CHIP,THR)

  Set a threshold value of specific PASTTREC

  :param str TDC: Address of associated TDC.
  :param int CONN: PASTTREC connection number.
  :param int CHIP: PASTTREC chip number.
  :param int THR: New threshold value.

  .. include:: reset_warning_adm.rst

.. py:function:: set_threshold_for_board(TDC,conn,thresh, new_style = False)
 
  The same as :py:func:`set_threshold`, but for all PASTTRECs associated with specific TDC.

  :param str TDC: Address of associated TDC.
  :param int CONN: PASTTREC connection number. Not used if the new style is used.
  :param int thresh: New threshold value
  :param boolean new_style: If **false**, only PASTTRECs, associated with the specific connection, are affected. (for back compatibility only).
                If **true**, all PASTTRECs associated with specific TDC are affected.

.. py:function:: set_threshold_for_board_by_name(board_name,thresh, new_style = False)

  The same as :py:func:`set_threshold_for_board`.

  :param str board_name: Name of the associated board.
  :param int thresh: New threshold value.
  :param boolean new_style: If **false**, only PASTTRECs, associated with the specific connection, are affected. (for back compatibility only).
              If **true**, all PASTTRECs associated with specific TDC are affected.

.. py:function:: slow_control_test(board_name)

  Performs a slow control test for the specified board. 

  :param str board_name: Name of the target board.

  .. danger::
    This function was left unchanged during the modification of python modules for compatibility with old tools. However, for an unknown reason, after applying the high threshold, the values of channel states are always 0. This leads to a permanent fail result (returns 0) of this test.

.. py:function:: slow_control_test_boards(board_list)

  Performs a slow control test for specified boards. 

  :param list(str) board_list: Names of target boards.

  .. danger::
    This function was left unchanged during the modification of python modules for compatibility with old tools. However, for an unknown reason, after applying the high threshold, the values of channel states are always 0. This leads to a permanent fail result (returns 0) of this test.

.. py:function:: set_baseline(TDC, channel,value)

  Set a baseline level for one of the TDC channels.

  :param str TDC: A TDC address.
  :param int channel: Number of the channel [0 - 31].
  :param int value: New value of baseline level [-15,15].

  .. include:: reset_warning_adm.rst

.. py:function:: set_all_baselines(TDC, channels, values)

  Set baseline levels for an array of TDC channels.

  :param str TDC: A TDC address.
  :param list(int) channels: Array of channel numbers.
  :param list(int) values: Array of new values of baseline levels.

.. py:function:: set_all_baselines_to_same_value(TDC, value)

  Set the same baseline level for all channels of specified TDC.

  :param str TDC: A TDC address.
  :param int value: New value of baseline levels..

.. py:function:: init_chip(TDC,CONN,CHIP,pktime,GAIN,thresh, baseline_sel=0b1)

  Performs an initialization of specific PASTTREC:

  1. Send a soft reset command
  2. Apply the specified peaking time, gain, and threshold.

  :param str TDC: An associated TDC.
  :param int CONN: A PASTTRECs connection number.
  :param int CHIP: A PASTTRECs chip number.
  :param int pktime: A value of peaking time, that will be applied.
  :param int GAIN: A value of gain, that will be applied.
  :param int thresh: A value of the threshold, that will be applied.
  :param int baseline_sel: A baseline selection option. For details see the PASTTREC documentation. Usually, should be the same as the default value.

.. py:function:: init_board(TDC,conn,pktime,gain,thresh, new_style=False, baseline_sel=0b1)

  The same as :py:func:`init_chip`, but for several PASTTRECs at once.

  :param str TDC: An associated TDC.
  :param int conn: A PASTTRECs connection number.
  :param int pktime: A value of peaking time, that will be applied.
  :param int gain: A value of gain, that will be applied.
  :param int thresh: A value of the threshold, that will be applied.
  :param boolean new_style: If **false**, only PASTTRECs, associated with the specific connection, are affected. (for back compatibility only).
              If **true**, all PASTTRECs associated with specific TDC are affected.
  :param int baseline_sel: A baseline selection option. For details see the PASTTREC documentation. Usually, should be the same as the default value.

.. py:function:: init_active_boards(pktime=-1,gain=-1,threshold=-1)

  The same as :py:func:`init_board`, but for all active boards.

  :param int pktime: A value of peaking time, that will be applied.
  :param int gain: A value of gain, that will be applied.
  :param int threshold: A value of the threshold, that will be applied.

.. py:function:: init_boards_by_name(board_list,pktime=-1,gain=-1,threshold=-1)

  The same as :py:func:`init_board`, but for a specified list of boards.

  :param list(str) board_list: A list of target board's names.
  :param int pktime: A value of peaking time, that will be applied.
  :param int gain: A value of gain, that will be applied.
  :param int threshold: A value of the threshold, that will be applied.

.. py:function:: init_board_by_name(board,pktime=-1,gain=-1,threshold=-1)

  The same as :py:func:`init_boards_by_name`, but for one board.

  :param str board: A target board name.
  :param int pktime: A value of peaking time, that will be applied.
  :param int gain: A value of gain, that will be applied.
  :param int threshold: A value of the threshold, that will be applied.

.. py:function:: found_baselines_for_board(board, scanning_time = 0.2, plot=False, apply_res=False)

  Found a baseline level for all channels of a specific board with a noise method.

  :param str board: A TDC address.
  :param float scanning_time: Duration of each step during data acquisition
  :param boolean plot: if true, plot the noise level versus baseline for each channel.
  :param boolean apply_res: if true, apply each baseline level to an estimated value.

  .. include:: reset_warning_adm.rst

.. py:function:: found_baselines_for_boards(boards, scanning_time = 0.2, plot=False, apply_res=False)

  The same as :py:func:`found_baselines_for_board` but for a list of boards.

  :param list(str) boards: A list of TDC's addresses.
  :param float scanning_time: Duration of each step during data acquisition.
  :param boolean plot: if true, plot the noise level versus baseline for each channel.
  :param boolean apply_res: if true, apply each baseline level to an estimated value.

.. py:function:: read_PASTTREC_regs(TDC_str, pasttrec)

  Read the values of PASTTREC registers.

  :param str TDC_str: A TDC address.
  :param int pasttrec: PASTTREC number.

  .. include:: reset_warning_adm.rst

  .. hint::
    The best way to read data from PASTTREC is using the :py:meth:`MDCTDC_FPGA.read_configuration` or :py:meth:`MDCTDC_PT.read_configuration` method.

