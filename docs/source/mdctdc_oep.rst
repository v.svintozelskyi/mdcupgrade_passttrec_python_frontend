Virtual OEP
===========

.. py:class:: MDCTDC_OEP
    
    A class for handling all the PASTTREC's configurations, associated with specific OEP.

    **Attributes**

    .. py:attribute:: addr
        :type: str

        Address of this TDC
    .. py:attribute:: fpga
        :type: list(MDCTDC_FPGA)

        An array of MDCTDC_FPGA objects.

    **Methods**
    
    .. py:method:: __init__(addr, fpga)
        
        Constructor

        :param str addr: A OEP address.
        :param fpga: Array of virtual connected MDCTDC_FPGA objects.
        :type fpga: list(:py:class:`MDCTDC_FPGA`).
        :returns: The instance of :py:class:`MDCTDC_FPGA` object.

    .. py:method:: resetConfig()

        Set all settings of all associated PASTTRECs to the default values.

        :returns: None