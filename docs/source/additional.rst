Additional classes
==================

.. py:class:: ConfStorage

    A class for storing prefilled sets of both PASTTREC settings and module memory content.

    **Attributes**

    .. py:attribute:: defaultPtSettings
        :type: dict(str, int)

        Values of some default PASTTREC registers, that are the same for the whole system.
        
        .. admonition:: Default value
        
            .. code-block:: python
        
                defaultPtSettings = {
                    "baseline_sel": 0b1,  
                    "tc1_c": 0b011,
                    "tc1_r": 0b110,
                    "tc2_c": 0b101,
                    "tc2_r": 0b010, 
                    "lvds_dac": 0b101
                }
    .. py:attribute:: memory_content_map
        :type: dict(str, dict(str, int))

        Default blocks of commands, that are flashed to module memory. The addr field corresponds to the address of a first command inside module memory. The len corresponds to the number of commands in a set.
    
        .. admonition:: Default value

            .. code-block:: python

                {
                    "blue_settings_pt10_g1_thr127":     { "addr": 0xC0, "len": 12},
                    "black_settings_pt20_g1_thr127":    { "addr": 0xD0, "len": 12},
                    "black_settings_pt15_g1_thr127":    { "addr": 0xE0, "len": 12},
                    "fast_past_reading":                { "addr": 0xF0, "len": 16},
                }

        
    .. py:attribute:: defaultPtMemory
        :type: dict(int, int)

        The actual default content of module memory. Each entry corresponds to the slow-control command for PASTTREC.

        .. admonition:: Default value

            .. code-block:: python

                {
                    0xC0 : 0x00050018, # blue_settings_pt10_g1_thr127
                    0xC1 : 0x0005011e,
                    0xC2 : 0x00050215,
                    0xC3 : 0x0005037f,
                    0xC4 : 0x0005040f,
                    0xC5 : 0x0005050f,
                    0xC6 : 0x0005060f,
                    0xC7 : 0x0005070f,
                    0xC8 : 0x0005080f,
                    0xC9 : 0x0005090f,
                    0xCa : 0x00050a0f,
                    0xCb : 0x00050b0f, # end blue_settings_pt10_g1_thr127
                    0xD0 : 0x0005001a, # black_settings_pt20_g1_thr127
                    0xD1 : 0x0005011e,
                    0xD2 : 0x00050215,
                    0xD3 : 0x0005037f,
                    0xD4 : 0x0005040f,
                    0xD5 : 0x0005050f,
                    0xD6 : 0x0005060f,
                    0xD7 : 0x0005070f,
                    0xD8 : 0x0005080f,
                    0xD9 : 0x0005090f,
                    0xDa : 0x00050a0f,
                    0xDb : 0x00050b0f, # end black_settings_pt20_g1_thr127
                    0xE0 : 0x00050019, # black_settings_pt15_g1_thr127
                    0xE1 : 0x0005011e,
                    0xE2 : 0x00050215,
                    0xE3 : 0x0005037f,
                    0xE4 : 0x0005040f,
                    0xE5 : 0x0005050f,
                    0xE6 : 0x0005060f,
                    0xE7 : 0x0005070f,
                    0xE8 : 0x0005080f,
                    0xE9 : 0x0005090f,
                    0xEa : 0x00050a0f,
                    0xEb : 0x00050b0f, # end black_settings_pt15_g1_thr127
                    0xF0 : 0x00051000, # fast pasttrec reading block
                    0xF1 : 0x00051100,
                    0xF2 : 0x00051200,
                    0xF3 : 0x00051300,
                    0xF4 : 0x00051400,
                    0xF5 : 0x00051500,
                    0xF6 : 0x00051600,
                    0xF7 : 0x00051700,
                    0xF8 : 0x00051800,
                    0xF9 : 0x00051900,
                    0xFA : 0x00051A00,
                    0xFB : 0x00051B00,
                    0xFC : 0x00051C00,
                    0xFD : 0x00051D00,
                    0xFE : 0x00051E00,
                    0xFF : 0x00051F00, # end fast pasttrec reading block
                }

.. py:class:: Helper

    A class contains useful functions, that can be used independently from all other code.

    **Methods**

    .. py:staticmethod:: getConnectedMDCtree()

        Generate a dictionary, that contains all connected OEP's and FPGA's addresses. It should be the same as in NetworkMap.
        
        :returns: Generated mdc_tree
        :rtype: dict(str, list(str))

        .. admonition:: Example
            
            Example output:
            
            .. code-block:: python

                {
                    '0x8012': {'0x1820': {}, '0x1821': {}},
                    '0x8001': {'0x1800': {}},
                    '0x8011': {'0x1818': {}, '0x1819': {}}
                }

            The first level keys are an OEP address (0x8012, 0x8001, 0x8011).
            The second level keys are an FPGA address (0x1820, 0x1821, etc)

    .. py:staticmethod:: getActiveMDCsubtree()

        The same as :py:meth:`getConnectedMDCtree`, but return only that boards that are not in standby mode. 

        :returns: Generated mdc_tree
        :rtype: dict(str, list(str))