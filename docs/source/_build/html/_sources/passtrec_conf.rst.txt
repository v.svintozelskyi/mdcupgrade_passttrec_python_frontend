PASTTREC configuration module (pasttrec_ctrl.py)
================================================

Contain the mechanisms for handling virtual PASTTREC configurations and effective flashing them.

.. toctree::
    MDCTDC_PT - Virtual PASTTREC objects <mdctdc_pt>
    MDCTDC_FPGA - Virtual PASTTREC objects <mdctdc_fpga>
    MDCTDC_OEP - Virtual PASTTREC objects <mdctdc_oep>

