.. warning::
    Any operation with PASTTREC ASIC should be done only **AFTER** the soft reset:
    
    * If an onboard memory was flashed using the corresponding tool from the :ref:`pasttrec_conf` (this means that PASTTREC is configured automatically during the power cycle), reset will be done **automatically**.
    * Use the :py:func:`reset_board` directly.
    * Use any of initialization functions: :py:func:`init_chip`, etc.
    
    It's enough to perform this operation only once after every power cycle.